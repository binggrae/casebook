<?php


namespace core\parser\forms;


use yii\base\Model;

/**
 * Class LoginForm
 * @package core\forms\power
 * @property array $postData
 */
class LoginForm extends Model
{

    public $username;
    public $password;

    public $token;

    public function rules()
    {
        return [
            [['username', 'password'], 'required'],
        ];
    }

    public function getPostData()
    {
        return [
            'UserName' => $this->username,
            'Password' => $this->password,
            'RememberMe' => 1,
            'SystemName' => 'Sps',
        ];
    }
}