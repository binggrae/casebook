<?php

namespace frontend\controllers;

use core\entities\Account;
use core\forms\AccountForm;
use core\parser\jobs\RegisterJob;
use Yii;
use yii\data\ActiveDataProvider;
use yii\filters\VerbFilter;
use yii\web\Controller;
use yii\web\NotFoundHttpException;

/**
 * AccountController implements the CRUD actions for Account model.
 */
class AccountController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }


    public function actionIndex()
    {
        $dataProvider = new ActiveDataProvider([
            'query' => Account::find(),
            'sort' => [
                'defaultOrder' => ['id' => SORT_DESC]
            ]
        ]);

        return $this->render('index', [
            'dataProvider' => $dataProvider,
        ]);
    }

    public function actionMain($id)
    {
        if (Yii::$app->request->isPost) {
            Account::setMain($id, true);
        }

        return $this->redirect(['account/index']);
    }


    public function actionCreate()
    {
        $form = new AccountForm();

        if ($form->load(Yii::$app->request->post()) && $form->validate()) {
            $account = Account::create($form);
            $account->save();

            Account::setMain($account->id, $account->is_main);

            return $this->redirect(['index']);
        }

        return $this->render('create', [
            'model' => $form,
        ]);
    }

    public function actionRegister()
    {
        $model = new Account([
            'login' => 'temp',
            'password' => 'temp',
            'is_main' => 0,
            'created_at' => time()
        ]);
        $model->save();

        Yii::$app->queue->push(new RegisterJob([
            'id' => $model->id
        ]));

        return $this->redirect(['/account/index']);
    }


    public function actionUpdate($id)
    {
        $account = $this->findModel($id);
        $form = new AccountForm($account);

        if ($form->load(Yii::$app->request->post()) && $form->validate()) {
            $account->edit($form);
            $account->save();

            Account::setMain($account->id, $account->is_main);

            return $this->redirect(['index']);
        }

        return $this->render('update', [
            'model' => $form,
            'account' => $account,
        ]);
    }


    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }


    protected function findModel($id)
    {
        if (($model = Account::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }
}
