<?php

use yii\db\Migration;

/**
 * Class m180206_211037_up
 */
class m180206_211037_up extends Migration
{
    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $this->addColumn('org_request', 'comment', $this->string());
        $this->addColumn('org_request', 'sum', $this->string());
        $this->addColumn('org_request', 'date', $this->string());
        $this->addColumn('org_request', 'name', $this->string());
        $this->addColumn('org_request', 'number', $this->string());
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        echo "m180206_211037_up cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m180206_211037_up cannot be reverted.\n";

        return false;
    }
    */
}
