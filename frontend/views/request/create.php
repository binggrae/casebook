<?php

use core\helpers\RequestHelper;
use yii\bootstrap\ActiveForm;
use yii\helpers\Html;
use yii\jui\DatePicker;

/* @var $this yii\web\View */
/* @var $model core\forms\RequestForm */

$this->title = 'Создание выборки';
$this->params['breadcrumbs'][] = ['label' => 'Поисковые выборки', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="request-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?php $form = ActiveForm::begin(); ?>
    <?= $form->field($model, 'sum')->textInput() ?>

    <?= $form->field($model, 'type')->dropDownList(RequestHelper::getTypeList()) ?>

    <h4>Дата регистрации</h4>
    <div class="row">
        <div class="col-md-6">
            <?= $form->field($model, 'date_from')->widget(DatePicker::className(), [
                'language' => 'ru',
                'dateFormat' => 'yyyy-MM-dd',
                'options' => [
                    'class' => 'form-control'
                ]
            ]) ?>
        </div>
        <div class="col-md-6">
            <?= $form->field($model, 'date_to')->widget(DatePicker::className(), [
                'language' => 'ru',
                'dateFormat' => 'yyyy-MM-dd',
                'options' => [
                    'class' => 'form-control'
                ]
            ]) ?>
        </div>
    </div>

    <div class="form-group">
        <?= Html::submitButton('Найти', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
