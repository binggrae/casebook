<?php


namespace core\parser\jobs;


use core\entities\Account;
use core\forms\AccountForm;
use core\parser\forms\RegisterForm;
use core\services\Client;
use core\services\TempMail;
use jumper423\sms\error\SmsException;
use jumper423\sms\Sms;
use yii\base\BaseObject;
use yii\queue\JobInterface;
use yii\queue\RetryableJobInterface;

class RegisterJob extends BaseObject implements JobInterface, RetryableJobInterface
{

    public $id;

    public function getTtr()
    {
        return 15*60;
    }

    public function canRetry($attempt, $error)
    {
        return ($attempt < 5);
    }



    /**
     * @param \yii\queue\Queue $queue
     * @throws \Exception
     */
    public function execute($queue)
    {
        /** @var Client $client */
        $client = \Yii::$container->get(Client::class);

        /** @var Sms $sms */
        $sms = \Yii::$container->get(Sms::class);

        /** @var TempMail $mail */
        $mail = \Yii::$container->get(TempMail::class);

        do {
            try {
                $number = $sms->getNumber();
                var_dump($number);
                $response = $client->post('https://casebook.ru/api/Account/SendPin?phone=' . $number, [])->send();

                if (!$response->data['Success']) {
                    $sms->setStatus($sms::STATUS_USED);
                    continue;
                }

                $sms->setStatus($sms::STATUS_READY);
                list($status, $code) = $sms->getCode();
                if ($status) {
                    $code = trim(explode(':', $code)[1]);
                    var_dump($code);
                    $sms->setStatus($sms::STATUS_COMPLETE);
                    $email = $mail->getEmail();

                    $form = new RegisterForm([
                        'email' => $email,
                        'code' => $code,
                        'phone' => $number,
                    ]);
                    $client->post('https://casebook.ru/api/Account/Register', $form->getPostData())->send();

                    $data = $mail->getMailData($email);
                    $client->get($data['link'])->send();


                    $model = Account::find()->where(['id' => $this->id])->one();
                    $form = new AccountForm($model, [
                        'login' => $email,
                        'password' => $data['pass'],
                        'is_main' => true
                    ]);

                    if ($model) {
                        $model->edit($form);
                    } else {
                        $model = Account::create($form);

                    }
                    if($model->save()) {
                        Account::setMain($model->id, $model->is_main);
                        return;
                    }
                }
            } catch (SmsException $e) {
                if ($e->getMessage() != 'Не нашло номер') {
                    $sms->setStatus($sms::STATUS_CANCEL);
                    throw $e;
                } else {
                    var_dump('not_number');
                }
            } catch (\Exception $e) {
                throw $e;
            }
            var_dump('sleep');
            sleep(5);
        } while (true);
    }


}