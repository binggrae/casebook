<?php


namespace core\helpers;


class AccountHelper
{

    public static function isMainLabel($is_main)
    {
        return self::isMainMap($is_main)['label'];
    }

    public static function isMainClass($is_main)
    {
        return self::isMainMap($is_main)['class'];
    }

    public static function isMainMap($value = null)
    {
        $map = [
            0 => [
                'label' => 'Нет',
                'class' => 'warning'
            ],
            1 => [
                'label' => 'Да',
                'class' => 'success'
            ]
        ];

        return is_null($value) ? $map : $map[$value];
    }

}