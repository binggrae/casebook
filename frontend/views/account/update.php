<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model core\forms\AccountForm */
/* @var $account core\entities\Account */

$this->title = 'Редактирование аккаунта: ' . $account->login;
$this->params['breadcrumbs'][] = ['label' => 'Аккаунты', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="account-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
