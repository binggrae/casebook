<?php

use core\entities\Organization;
use core\entities\Request;
use yii\grid\GridView;
use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $dataProvider yii\data\ActiveDataProvider */
/* @var $model Request */

$this->title = 'Выборка от ' . date('d.m.Y H:i:s', $model->created_at);
$this->params['breadcrumbs'][] = ['label' => 'Поисковые выборки', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;

?>
<div class="account-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('ОГРН', ['request/set-ogrn', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Скачать', ['request/download', 'id' => $model->id], ['class' => 'btn btn-success']) ?>
    </p>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'columns' => [
            [
                'attribute' => 'orgRequests.number',
                'value' => function (Organization $org) use ($model) {
                    foreach ($org->orgRequests as $orgRequest) {
                        if ($orgRequest->request_id == $model->id) {
                            return $orgRequest->number;
                        }
                    }
                    return null;
                }
            ],
            [
                'attribute' => 'orgRequests.date',
                'value' => function (Organization $org) use ($model) {
                    foreach ($org->orgRequests as $orgRequest) {
                        if ($orgRequest->request_id == $model->id) {
                            return Yii::$app->formatter->asDate($orgRequest->date, 'short');
                        }
                    }
                    return null;
                }
            ],
            [
                'attribute' => 'orgRequests.name',
                'value' => function (Organization $org) use ($model) {
                    foreach ($org->orgRequests as $orgRequest) {
                        if ($orgRequest->request_id == $model->id) {
                            return $orgRequest->name;
                        }
                    }
                    return null;
                }
            ],
            [
                'attribute' => 'orgRequests.comment',
                'value' => function (Organization $org) use ($model) {
                    foreach ($org->orgRequests as $orgRequest) {
                        if ($orgRequest->request_id == $model->id) {
                            return $orgRequest->comment;
                        }
                    }
                    return null;
                }
            ],
            [
                'attribute' => 'orgRequests.sum',
                'value' => function (Organization $org) use ($model) {
                    foreach ($org->orgRequests as $orgRequest) {
                        if ($orgRequest->request_id == $model->id) {
                            return Yii::$app->formatter->asCurrency($orgRequest->sum);
                        }
                    }
                    return null;
                }
            ],
            [
                'attribute' => 'ogrn',
                'format' => 'raw',
                'value' => function (Organization $model) {
                    return $model->ogrn ?
                        Html::a($model->ogrn, 'https://casebook.ru/#side/info/' . $model->ogrn, [
                            'target' => '_blank'
                        ]) :
                        Yii::$app->view->render('_form', ['model' => $model]);
                }
            ],

            [
                'attribute' => 'name',
                'format' => 'raw',
                'value' => function (Organization $model) {
                    return ($model->ogrn ? $model->name :
                            Html::a($model->name, 'http://www.rusprofile.ru/search?query='.$model->name, [
                                'target' => '_blank'
                            ])) .
                        "<br><br>" . $model->address;
                }
            ],
            'email',
            'phone',
            'chief',
//            [
//                'attribute' => 'status',
//                'format' => 'raw',
//                'value' => function (Organization $model) {
//                    return Html::tag('span', OrgHelper::getStatusLabel($model->status), [
//                        'class' => 'label ' . OrgHelper::getStatusClass($model->status)
//                    ]);
//                }
//            ],
        ],
    ]); ?>
</div>
