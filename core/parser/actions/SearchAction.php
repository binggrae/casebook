<?php


namespace core\parser\actions;


use core\entities\Organization as OrganizationModel;
use core\entities\OrgRequest;
use core\exceptions\FailedLoginException;
use core\parser\Api;
use core\parser\forms\SearchForm;
use core\parser\jobs\LoadJob;
use core\parser\pages\SearchPage;
use core\services\Client;

class SearchAction
{

    /** @var Client */
    private $client;

    /** @var Api */
    private $api;

    public function __construct(Client $client)
    {
        $this->client = $client;
    }

    private function getApi()
    {
        if (!$this->api) {
            $this->api = \Yii::$container->get(Api::class);
        }

        return $this->api;
    }

    public function run(SearchForm $form)
    {
        foreach ($this->search($form) as $page) {
            if (!$page->isSuccess()) {
                if ($this->getApi()->login()) {
                    $page->setContinue();
                } else {
                    throw new FailedLoginException('Ошибка авторизации');
                }
                continue;
            }

            $load_ids = [];
            foreach ($page->getList() as $item) {
                /** @var OrganizationModel $model */
                if ($item->ogrn) {
                    $model = OrganizationModel::find()->where(['ogrn' => $item->ogrn])->one();
                } else {
                    $model = OrganizationModel::find()->where(['name' => $item->name])->one();
                }

                if (!$model) {
                    $model = OrganizationModel::create($item);
                    $model->save();

                    $load_ids[] = $model->id;
                }

                $rel = OrgRequest::find()->where(['org_id' => $model->id, 'request_id' => $form->getId()])->one();
                if (!$rel) {
                    $rel = new OrgRequest([
                        'org_id' => $model->id,
                        'request_id' => $form->getId(),
                        'comment' => $item->comment,
                        'name' => $item->instance,
                        'sum' => $item->sum,
                        'date' => $item->date,
                        'number' => $item->number
                    ]);
                    $rel->save();
                }
            }

            if (count($load_ids)) {
                \Yii::$app->queue->push(new LoadJob([
                    'ids' => $load_ids
                ]));
            }

            $form->page++;
        }

        return true;
    }

    /**
     * @param SearchForm $form
     * @return \Generator|SearchPage[]
     */
    public function search(SearchForm &$form)
    {
        do {
            $response = $this->client->post(SearchPage::URL, $form->getPostData())->send();
            $page = new SearchPage($response->data);

            yield $page;
        } while ($page->hasNext());
    }

}