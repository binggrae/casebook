<?php
return [
    'aliases' => [
        '@bower' => '@vendor/bower-asset',
        '@npm'   => '@vendor/npm-asset',
    ],
    'vendorPath' => dirname(dirname(__DIR__)) . '/vendor',
    'bootstrap' => [
        'common\bootstrap\SetUp',
        'queue'
    ],
    'language' => 'ru-RU',
    'timeZone' => 'Europe/Moscow',
    'components' => [
        'cache' => [
            'class' => 'yii\caching\FileCache',
            'cachePath' => '@common/runtime/cache',
        ],
        'queue' => [
            'class' => '\yii\queue\db\Queue',
            'db' => 'db',
            'tableName' => '{{%queue}}',
            'channel' => 'default',
            'mutex' => '\yii\mutex\MysqlMutex',
            'ttr' => 1000,
            'attempts' => 3,
        ],
        'sms' => [
            'class' => \jumper423\sms\Sms::className(),
            'site' => \jumper423\sms\service\SmsSites::AOL,
            'services' => [
                [
                    'class' => \jumper423\sms\service\SmsActivateService::className(),
                    'apiKey' => '74fd51d1d30ee3B631855ed2573c3812',
                ]
            ],
        ],
    ],
];
