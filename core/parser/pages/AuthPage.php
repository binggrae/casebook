<?php

namespace core\parser\pages;

class AuthPage
{
    const URL = 'https://casebook.ru/api/Account/LogOn';

    private $json;


    public function __construct($json)
    {
        $this->json = $json;
    }

    public function isSuccess()
    {
        return $this->json['Success'];
    }
}