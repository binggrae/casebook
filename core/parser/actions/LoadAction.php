<?php


namespace core\parser\actions;


use core\entities\Organization;
use core\helpers\OrgHelper;
use core\parser\Api;
use core\parser\pages\ViewPage;
use core\services\Client;

class LoadAction
{

    /** @var Client */
    private $client;

    /** @var Api */
    private $api;

    public function __construct(Client $client)
    {
        $this->client = $client;
    }

    private function getApi()
    {
        if (!$this->api) {
            $this->api = \Yii::$container->get(Api::class);
        }

        return $this->api;
    }

    /**
     * @param Organization[] $models
     */
    public function run($models)
    {
        $is_load = true;
        do {
            if (!$is_load) {
                $this->getApi()->login();
            }

            $requests = [];
            foreach ($models as $model) {
                if (!$model->ogrn) {
                    unset($models[$model->id]);
                    continue;
                }
                $requests[$model->id] = $this->client->post(ViewPage::URL, ['ogrn' => $model->ogrn]);
            }

            $responses = $this->client->batch($requests);

            foreach ($responses as $id => $response) {
                $card = new ViewPage($response->data);
                if (!$card->isSuccess()) {
                    $is_load = false;
                    continue;
                }
                $models[$id]->email = $card->getEmail();
                $models[$id]->phone = $card->getPhone();
                $models[$id]->chief = $card->getChief();
                $models[$id]->status = OrgHelper::STATUS_COMPLETE;

                if (!$models[$id]->chief) {
                    $models[$id]->chief = $this->loadChief($models[$id]->ogrn);
                }

                $models[$id]->save($models[$id]->ogrn);
                unset($models[$id]);
            }
        } while (count($models));
    }


    public function loadChief($ogrn)
    {
        $response = $this->client->post(ViewPage::CHIEF_URL, [
            'ogrn' => $ogrn,
            'page' => 1,
            'count' => 10
        ])->send();

        if(!$response->data['Success']) {
            return null;
        }

        foreach ($response->data['Result']['Items'] as $item) {
            if($item['Status'] === 0) {
                return $item['ShortName'];
            }
        }

        return null;
    }

}