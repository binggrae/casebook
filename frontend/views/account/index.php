<?php

use core\entities\Account;
use core\helpers\AccountHelper;
use yii\grid\GridView;
use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Аккаунты';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="account-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Добавить', ['create'], ['class' => 'btn btn-success']) ?>
        <?= Html::a('Регистрация', ['register'], ['class' => 'btn btn-success']) ?>
    </p>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'columns' => [
            'id',
            'login',
            'password',
            'created_at:datetime',
            [
                'attribute' => 'is_main',
                'format' => 'raw',
                'value' => function (Account $account) {
                    return Html::a(AccountHelper::isMainLabel($account->is_main), ['account/main', 'id' => $account->id], [
                        'class' => 'btn btn-xs btn-' . AccountHelper::isMainClass($account->is_main),
                        'data' => [
                            'method' => 'post'
                        ]
                    ]);
                }
            ],

            [
                'class' => 'yii\grid\ActionColumn',
                'template' => '{update} {delete}'
            ],
        ],
    ]); ?>
</div>
