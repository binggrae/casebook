<?php


namespace core\parser\pages;


use core\parser\elements\Organization;
use yii\helpers\VarDumper;

class SearchPage
{

    private $continue = false;

    const URL = 'https://casebook.ru/api/Search/Cases';

    private $json;


    public function __construct($json)
    {
        $this->json = $json;
    }

    public function isSuccess()
    {
        return $this->json['Success'];
    }

    public function setContinue()
    {
        $this->continue = true;
    }

    public function hasNext()
    {
        return
            $this->continue ||
            (isset($this->json['Result']['Items']) && (bool)count($this->json['Result']['Items']));
    }


    /**
     * @return \Generator|Organization[]
     */
    public function getList()
    {
        foreach ($this->json['Result']['Items'] as $item) {
            $instance = $item['Instances'][count($item['Instances']) - 1]['Court'];

            foreach ($item['Sides'] as $side) {
                if($side['Type'] == 1) {
                    if($side['IsPhysical']) {
                        continue;
                    }
                    yield  new Organization(
                        $side['Ogrn'],
                        $side['Name'],
                        $side['Address'],
                        $item['Comment'],
                        $item['ClaimSum'],
                        $item['StartDate'],
                        $instance,
                        $item['CaseNumber']
                    );
                }
            }
        }
    }

}