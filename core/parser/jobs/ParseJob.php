<?php


namespace core\parser\jobs;

use core\entities\Request;
use core\helpers\RequestHelper;
use core\parser\Api;
use core\parser\forms\SearchForm;
use yii\base\BaseObject;
use yii\queue\JobInterface;

class ParseJob extends BaseObject implements JobInterface
{

    public $id;
    /**
     * @var Api
     */
    private $api;

    public function __construct(array $config = [])
    {
        parent::__construct($config);
    }

    /**
     * @param \yii\queue\Queue $queue
     * @throws \Exception
     */
    public function execute($queue)
    {
        $this->api = \Yii::$container->get(Api::class);

        $request = Request::find()->where(['id' => $this->id])->one();

        if($request) {
            $request->status = RequestHelper::STATUS_PROGRESS;
            $request->save();
            $searchForm = new SearchForm($request);
            if($this->api->search($searchForm)) {
                $request->status = RequestHelper::STATUS_COMPLETE;
            } else {
                $request->status = RequestHelper::STATUS_ERROR;
            }
            $request->save();
        }
    }


}