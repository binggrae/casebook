<?php


namespace core\parser\forms;


use core\entities\Request;
use core\helpers\RequestHelper;
use yii\base\Model;
use yii\helpers\Json;

class SearchForm extends Model
{

    public $accuracy = 2;
    public $bankruptStages = [];
    public $caseCategoryId = "";
    public $caseCategoryIds = [];
    public $caseResults = [];
    public $caseTypes = ["Г"];
    public $coSides = [];
    public $considerType = -1;
    public $count = 20;
    public $courtType = -1;
    public $courts = ["ASMO", "MSK"];
    public $dateFrom = null;
    public $dateTo = null;
    public $delegate = "";
    public $finalDocFrom = null;
    public $finalDocTo = null;
    public $generalCaseTypes = [];
    public $generalCourts = [];
    public $instances = [1];
    public $isCasesTracking = true;
    public $isMessageFrTracking = true;
    public $isNeedAccuracy = 2;
    public $isNeedBankruptStageTracking = false;
    public $isNeedBflTracking = true;
    public $isNeedCaseModificationTracking = false;
    public $isNeedFnsDocsTracking = false;
    public $isNeedFnsRequisitesTracking = true;
    public $isNeedFnsTracking = true;
    public $isNeedIntendedBankruptcyTracking = false;
    public $isNeedMAClaims = true;
    public $isNeedNewCasesTracking = true;
    public $isNeedWritsTracking = false;
    public $judges = [];
    public $judgesNames = [];
    public $maxSum = -1;
    public $minSum = 500000;
    public $monitoredStatus = -1;
    public $orderBy = "incoming_date_ts desc, case_number desc";
    public $page = 1;
    public $query = "";
    public $sessionFrom = null;
    public $sessionTo = null;
    public $sideTypes = [];

    public $sides = [];

    public $stateOrganizations = [];

    public $statusEx = [];

    /** @var Request */
    private $request;

    public function __construct(Request $request, array $config = [])
    {
        $this->finalDocFrom= $request->date_from;
        $this->finalDocTo = $request->date_to;
        $this->minSum = $request->sum;
        $this->caseResults = RequestHelper::getTypeData($request->type);

        $this->request  = $request;

        parent::__construct($config);
    }

    public function getId()
    {
        return $this->request->id;
    }

    public function getPostData()
    {
        return get_object_vars($this);
    }

}