<?php


namespace frontend\controllers;


use core\entities\Organization;
use core\entities\Request;
use core\forms\RequestForm;
use core\helpers\OrgHelper;
use core\helpers\RequestHelper;
use core\parser\jobs\LoadJob;
use core\parser\jobs\ParseJob;
use yii\data\ActiveDataProvider;
use yii\helpers\VarDumper;
use yii\web\Controller;
use yii\web\NotFoundHttpException;

class RequestController extends Controller
{

    public function actionIndex()
    {

        $dataProvider = new ActiveDataProvider([
            'query' => Request::find(),
            'sort' => [
                'defaultOrder' => ['id' => SORT_DESC]
            ]
        ]);

        return $this->render('index', [
            'dataProvider' => $dataProvider,
        ]);
    }


    public function actionCreate()
    {
        $form = new RequestForm();

        if ($form->load(\Yii::$app->request->post()) && $form->validate()) {
            $request = Request::create($form);
            $request->save();

            \Yii::$app->queue->push(new ParseJob([
                'id' => $request->id
            ]));

            return $this->redirect(['index']);
        }

        return $this->render('create', [
            'model' => $form,
        ]);
    }

    public function actionOgrn($id)
    {
        $model = Organization::find()->where(['id' => $id])->one();
        if (!$model) {
            throw new NotFoundHttpException('Организация не найдена');
        }

        if ($model->load(\Yii::$app->request->post())) {
            $model->status = OrgHelper::STATUS_WAIT;
            $model->save();

            \Yii::$app->queue->push(new LoadJob([
                'ids' => [$model->id]
            ]));
        }

        return $this->redirect(\Yii::$app->request->referrer);
    }

    public function actionSetOgrn($id)
    {
        $model = $this->findModel($id);
        $dataProvider = new ActiveDataProvider([
            'query' => $model->getRequests()->where(['ogrn' => ''])->with('orgRequests'),
        ]);

        return $this->render('view', [
            'model' => $model,
            'dataProvider' => $dataProvider,
        ]);
    }

    public function actionView($id)
    {
        $model = $this->findModel($id);
        $dataProvider = new ActiveDataProvider([
            'query' => $model->getRequests()->with('orgRequests'),
        ]);

        return $this->render('view', [
            'model' => $model,
            'dataProvider' => $dataProvider,
        ]);
    }

    public function actionDownload($id)
    {
        $model = $this->findModel($id);
        $query = Organization::find();
        if ($id) {
            $query->select(['org_request.*', 'organization.*']);
            $query->joinWith(['orgRequest' => function ($query) use ($id){
                return $query->where(['request_id' => $id]);

            }]);

        }

        $file = \Yii::createObject([
            'class' => 'codemix\excelexport\ExcelFile',
            'sheets' => [
                'Person' => [
                    'class' => 'codemix\excelexport\ActiveExcelSheet',
                    'query' => $query,
                    'types' => ['s', 's', 's', 's', 's', 's', 's'],
                    'attributes' => [
                        'orgRequest.number',
                        'orgRequest.date',
                        'orgRequest.name',
                        'orgRequest.comment',
                        'orgRequest.sum',
                        'ogrn',
                        'name',
                        'address',
                        'email',
                        'phone',
                        'chief'
                    ],
                ]
            ]
        ]);
        $file->send('Выборка '. RequestHelper::getTypeValue($model->type). ' от ' . date('d.m.Y H:i:s', $model->created_at) . '.xlsx');
    }

    public function actionDelete($id)
    {
        $model = $this->findModel($id);

        $model->delete();

        return $this->redirect(['index']);
    }


    /**
     * @param $id
     * @return array|Request|null|\yii\db\ActiveRecord
     * @throws NotFoundHttpException
     */
    private function findModel($id)
    {
        $model = Request::find()->where(['id' => $id])->one();
        if (!$model) {
            throw new NotFoundHttpException('Выборка не найдена');
        }

        return $model;
    }


}