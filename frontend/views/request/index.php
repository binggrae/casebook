<?php

use core\entities\Request;
use core\helpers\RequestHelper;
use yii\grid\GridView;
use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Поисковые выборки';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="account-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Сделать выборку', ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'columns' => [
            'id',
            [
                'attribute' => 'created_at',
                'format' => 'raw',
                'value' => function (Request $request) {
                    return Html::a(\Yii::$app->formatter->asDatetime($request->created_at),
                        ['view', 'id' => $request->id]);
                }
            ],
            'sum',
            [
                'attribute' => 'type',
                'format' => 'raw',
                'value' => function (Request $request) {
                    return (string)RequestHelper::getTypeValue($request->type);
                }
            ],

            [
                'header' => 'Дата регистрации',
                'format' => 'raw',
                'value' => function (Request $request) {
                    return 'с ' . $request->date_from . ' по ' . $request->date_to;
                }
            ],
            [
                'header' => 'Найдено / Загружено',
                'format' => 'raw',
                'value' => function (Request $request) {
                    return count($request->requests) . ' / ' . count($request->completeRequests);
                }
            ],
            [
                'attribute' => 'status',
                'format' => 'raw',
                'value' => function (Request $model) {
                    return Html::tag('span', RequestHelper::getStatusLabel($model->status), [
                        'class' => 'label ' . RequestHelper::getStatusClass($model->status)
                    ]);
                }
            ],

            [
                'class' => 'yii\grid\ActionColumn',
                'template' => '{view} {download} {delete}',
                'buttons' => [
                    'download' => function ($link) {
                        return Html::a('<span class="glyphicon glyphicon-save"></span>',
                            $link, [
                                'title' => \Yii::t('yii', 'Скачать'),
                                'data-pjax' => '0',
                            ]);
                    },
                ]
            ],
        ],
    ]); ?>
</div>
