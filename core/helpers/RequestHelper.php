<?php


namespace core\helpers;


class RequestHelper
{
    const STATUS_WAIT = 5;
    const STATUS_PROGRESS = 10;
    const STATUS_ERROR = 15;
    const STATUS_COMPLETE = 20;

    const TYPE_GOOD = 0;
    const TYPE_FAIL = 5;
    const TYPE_ALL = 10;

    public static function getStatusLabel($status = null)
    {
        $labels = [
            self::STATUS_WAIT => 'Ожидает запуска',
            self::STATUS_PROGRESS => 'В работе',
            self::STATUS_ERROR => 'Ошибка',
            self::STATUS_COMPLETE => 'Завершено',
        ];

        return is_null($status) ? $labels : $labels[$status];
    }


    public static function getStatusClass($status)
    {
        $classes = [
            self::STATUS_WAIT => 'label-warning',
            self::STATUS_PROGRESS => 'label-primary',
            self::STATUS_ERROR => 'label-danger',
            self::STATUS_COMPLETE => 'label-success',
        ];

        return $classes[$status];
    }

    public static function getTypeList()
    {
        return [
            self::TYPE_GOOD => 'Иск удовлетворен или частично',
            self::TYPE_FAIL => 'Иск не уволетворен или возвращен',
            self::TYPE_ALL => 'Недавно поданные иски',
        ];
    }

    public static function getTypeValue($type = null)
    {
        $data = [
            self::TYPE_GOOD => 'Иск удовлетворен или частично',
            self::TYPE_FAIL => 'Иск не уволетворен или возвращен',
            self::TYPE_ALL => 'Недавно поданные иски',
        ];

        return !is_null($type) && isset($data[$type]) ? $data[$type] : null;
    }

    public static function getTypeData($type)
    {
        $data = [
            self::TYPE_GOOD => [0, 1],
            self::TYPE_FAIL => [2, 3],
            self::TYPE_ALL => [],
        ];

        return isset($data[$type]) ? $data[$type] : null;
    }

}