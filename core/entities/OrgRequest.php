<?php

namespace core\entities;

use Yii;

/**
 * This is the model class for table "org_request".
 *
 * @property int $request_id
 * @property int $org_id
 * @property string $comment
 * @property string $sum
 * @property string $date
 * @property string $name
 * @property string $number
 *
 * @property Organization $org
 * @property Request $request
 */
class OrgRequest extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'org_request';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['request_id', 'org_id'], 'required'],
            [['request_id', 'org_id'], 'integer'],
            [['comment', 'sum', 'date', 'name', 'number'], 'string', 'max' => 255],
            [['org_id'], 'exist', 'skipOnError' => true, 'targetClass' => Organization::className(), 'targetAttribute' => ['org_id' => 'id']],
            [['request_id'], 'exist', 'skipOnError' => true, 'targetClass' => Request::className(), 'targetAttribute' => ['request_id' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'request_id' => 'Request ID',
            'org_id' => 'Org ID',
            'comment' => 'Комментарий',
            'sum' => 'Сумма',
            'date' => 'Дата начала',
            'name' => 'Название суда',
            'number' => 'Номер дела',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getOrg()
    {
        return $this->hasOne(Organization::className(), ['id' => 'org_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getRequest()
    {
        return $this->hasOne(Request::className(), ['id' => 'request_id']);
    }
}
