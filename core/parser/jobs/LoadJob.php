<?php


namespace core\parser\jobs;

use core\entities\Organization;
use core\helpers\OrgHelper;
use core\parser\Api;
use yii\base\BaseObject;
use yii\queue\JobInterface;

class LoadJob extends BaseObject implements JobInterface
{

    public $ids;
    /**
     * @var Api
     */
    private $api;

    public function __construct(array $config = [])
    {
        parent::__construct($config);
    }

    /**
     * @param \yii\queue\Queue $queue
     * @throws \Exception
     */
    public function execute($queue)
    {
        $this->api = \Yii::$container->get(Api::class);

        /** @var Organization[] $models */
        $models = Organization::find()
            ->andWhere(['id' => $this->ids])
            ->andWhere(['not', ['ogrn' => null]])
            ->indexBy('id')->all();
        Organization::updateAll(['status' => OrgHelper::STATUS_PROGRESS], ['id' => $this->ids]);

        $this->api->load($models);

        Organization::updateAll(['status' => OrgHelper::STATUS_COMPLETE], ['id' => $this->ids]);
    }


}