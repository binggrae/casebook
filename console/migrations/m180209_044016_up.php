<?php

use yii\db\Migration;

/**
 * Class m180209_044016_up
 */
class m180209_044016_up extends Migration
{
    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $this->addColumn('request', 'sum', $this->integer()->notNull()->defaultValue(300000));
        $this->addColumn('request', 'type', $this->integer());

    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        echo "m180209_044016_up cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m180209_044016_up cannot be reverted.\n";

        return false;
    }
    */
}
