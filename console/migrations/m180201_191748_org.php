<?php

use yii\db\Migration;

/**
 * Class m180201_191748_org
 */
class m180201_191748_org extends Migration
{
    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $this->renameColumn('organization', 'orgn', 'ogrn');

        $this->addColumn('organization', 'status' , $this->integer());
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        echo "m180201_191748_org cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m180201_191748_org cannot be reverted.\n";

        return false;
    }
    */
}
