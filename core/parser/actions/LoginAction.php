<?php


namespace core\parser\actions;


use core\entities\Account;
use core\exceptions\RequestException;
use core\parser\forms\LoginForm;
use core\parser\pages\AuthPage;
use core\services\Client;

class LoginAction
{
    /**
     * @var Client
     */
    private $client;

    public function __construct(Client $client)
    {
        $this->client = $client;
    }

    /**
     * @return bool
     */
    public function run()
    {
        $account = Account::find()->where(['is_main' => 1])->one();
        $form = new LoginForm([
            'username' => $account->login,
            'password' => $account->password,
        ]);

        $request = $this->client->post(AuthPage::URL, $form->getPostData())->send();
        if ($request->isOk) {
            $page = new AuthPage($request->data);

            return $page->isSuccess();
        } else {
            throw new RequestException('Failed load login page');
        }
    }

}