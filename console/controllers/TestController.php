<?php


namespace console\controllers;


use core\entities\Account;
use core\entities\Request;
use core\parser\forms\LoginForm;
use core\parser\forms\SearchForm;
use core\parser\Api;
use yii\console\Controller;

class TestController extends Controller
{

    /**
     * @var Api
     */
    private $api;

    public function __construct(string $id, $module, Api $api, array $config = [])
    {
        parent::__construct($id, $module, $config);
        $this->api = $api;
    }

    public function actionRun()
    {
        $request = Request::find()->one();
        $searchForm = new SearchForm($request);
        $this->api->search($searchForm);
    }


}