<?php


namespace core\parser\pages;


class ViewPage
{
    const URL = 'https://casebook.ru/api/Card/BusinessCard';

    const CHIEF_URL = 'https://casebook.ru/api/Card/GetChiefHistory';

    private $json;

    private $_load = true;

    public function __construct($json)
    {
        $this->json = $json;
    }

    public function isSuccess()
    {
        return $this->json['Success'];
    }

    public function getPhone()
    {
        return $this->json['Result']['Phone'] ?: null;
    }

    public function getEmail()
    {
        return $this->json['Result']['Email'] ?: null;
    }

    public function getChief()
    {
        return $this->json['Result']['Chief']['Name'] ?: null;
    }

    public function notLoad()
    {
        $this->_load = false;
    }

    public function hasLoad()
    {
        return $this->_load;
    }


}