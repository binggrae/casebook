<?php


namespace core\forms;


use core\helpers\RequestHelper;
use yii\base\Model;

class RequestForm extends Model
{

    public $date_from;

    public $date_to;

    public $sum;

    public $type;

    public function init()
    {
        $this->sum = 300000;
        $this->type = RequestHelper::TYPE_GOOD;

        $this->date_from = date('d.m.Y', time() - 60 * 60 * 24 * 3);
        $this->date_to = date('d.m.Y', time());
        parent::init();
    }

    public function rules()
    {
        return [
            [['sum', 'type'], 'integer'],
            [['date_from', 'date_to'], 'string'],
        ];
    }

    public function attributeLabels()
    {
        return [
            'date_from' => 'C',
            'date_to' => 'ПО',
            'sum' => 'Сумма',
            'type' => 'Результат',
        ];
    }


}