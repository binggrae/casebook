<?php


namespace console\controllers;


use core\entities\Account;
use core\forms\AccountForm;
use core\parser\forms\RegisterForm;
use core\services\Client;
use core\services\TempMail;
use jumper423\sms\error\SmsException;
use jumper423\sms\Sms;
use yii\console\Controller;

class AccountController extends Controller
{


    public function actionRegister()
    {
        /** @var Client $client */
        $client = \Yii::$container->get(Client::class);

        /** @var Sms $sms */
        $sms = \Yii::$container->get(Sms::class);

        /** @var TempMail $mail */
        $mail = \Yii::$container->get(TempMail::class);

        do {
            try {
                $number = $sms->getNumber();
                $response = $client->post('https://casebook.ru/api/Account/SendPin?phone=' . $number, [])->send();


                if (!$response->data['Success']) {
                    $sms->setStatus($sms::STATUS_USED);
                    continue;
                }

                $sms->setStatus($sms::STATUS_READY);
                list($status, $code) = $sms->getCode();
                if ($status) {
                    $code = trim(explode(':', $code)[1]);
                    $sms->setStatus($sms::STATUS_COMPLETE);
                    $email = $mail->getEmail();

                    $form = new RegisterForm([
                        'email' => $email,
                        'code' => $code,
                        'phone' => $number,
                    ]);
                    $client->post('https://casebook.ru/api/Account/Register', $form->getPostData())->send();

                    $data = $mail->getMailData($email);
                    $client->get($data['link'])->send();


                    $form = new AccountForm(null, [
                        'login' => $email,
                        'password' => $data['pass'],
                        'is_main' => true
                    ]);

                    $model = Account::create($form);
                    if ($model->save()) {
                        Account::setMain($model->id, $model->is_main);
                        return;
                    }
                }
            } catch (SmsException $e) {
                if ($e->getMessage() != 'Не нашло номер') {
                    $sms->setStatus($sms::STATUS_CANCEL);
                    throw $e;
                } else {
                    var_dump('not_number');
                }
            } catch (\Exception $e) {
                throw $e;
            }
            var_dump('sleep');
            sleep(5);
        } while (true);
    }

}