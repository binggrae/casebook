<?php


namespace core\forms;


use core\entities\Account;
use yii\base\Model;

class AccountForm extends Model
{
    public $login;
    public $password;
    public $is_main;

    public function __construct(Account $account = null, array $config = [])
    {
        if($account) {
            $this->login = $account->login;
            $this->password = $account->password;
            $this->is_main = $account->is_main;
        }
        parent::__construct($config);
    }

    public function rules()
    {
        return [
            [['login', 'password'], 'required'],
            [['is_main'], 'boolean'],
            [['is_main'], 'default', 'value' => false],
            [['login', 'password'], 'string', 'max' => 255],
        ];
    }


    public function attributeLabels()
    {
        return (new Account())->attributeLabels();
    }


}