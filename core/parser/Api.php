<?php


namespace core\parser;


use core\entities\Organization;
use core\parser\actions\LoadAction;
use core\parser\forms\SearchForm;
use core\parser\actions\LoginAction;
use core\parser\actions\SearchAction;

class Api
{
    /** @var LoginAction */
    private $login;
    /**
     * @var SearchAction
     */
    private $search;
    /**
     * @var LoadAction
     */
    private $load;


    public function __construct(
        LoginAction $loginAction,
        SearchAction $searchAction,
        LoadAction $loadAction
    )
    {
        $this->login = $loginAction;
        $this->search = $searchAction;
        $this->load = $loadAction;
    }

    public function login()
    {
        return $this->login->run();
    }


    public function search(SearchForm $form)
    {
        return $this->search->run($form);
    }

    /**
     * @param Organization[] $models
     */
    public function load($models)
    {

        $this->load->run($models);
    }

}