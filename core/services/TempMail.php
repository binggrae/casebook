<?php


namespace core\services;


use core\exceptions\RequestException;

class TempMail
{

    const PREFIX = 'casebook_';

    /**
     * @var Client
     */
    private $client;

    public function __construct(Client $client)
    {

        $this->client = $client;
    }


    public function getEmail()
    {
        $response = $this->client->get('http://api.temp-mail.ru/request/domains/format/json/')->send();

        if ($response->isOk) {
            return self::PREFIX . uniqid() . $response->data[0];
        } else {
            throw new RequestException('Temp Mail Error');
        }
    }


    public function getMailData($email)
    {
        while (1) {

            $response = $this->client->get('http://api.temp-mail.ru/request/source/id/' . md5($email) . '/format/json/')->send();

            if(isset($response->data['error'])) {
                sleep(5);
                var_dump('Mail sleep');
                continue;
            }
            foreach ($response->data as $mail) {
                $segments = explode("\r\n", explode("\r\n\r\n", $mail['mail_source'])[1]);

                $html = '';
                foreach ($segments as $segment) {
                    $html .= (base64_decode($segment));
                }

                preg_match('/Пароль: \<b class="strong" style="font-weight: bold;"\>(\w+)<\/b>/', $html, $pass);
                preg_match('/>http:\/\/casebook\.ru\/\/Account\/ValidateEmail\?(.+)<\/span>/', $html, $link);

                if (isset($pass[1]) && isset($link[1])) {
                    return [
                        'pass' => $pass[1],
                        'link' => str_replace('&amp;', '', 'http://casebook.ru/Account/ValidateEmail?' . $link[1])
                    ];
                }
            }
        }
        return [
            'pass' => '1',
            'link' => ''
        ];
    }

}