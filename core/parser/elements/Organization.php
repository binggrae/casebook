<?php


namespace core\parser\elements;


class Organization
{

    public $ogrn;

    public $name;

    public $address;

    public $phone;

    public $email;

    public $chief;

    public $comment;

    public $sum;

    public $date;

    public $instance;

    public $number;

    public function __construct(
        $ogrn,
        $name,
        $address,
        $comment,
        $sum,
        $date,
        $instance,
        $number
    )
    {
        $this->ogrn = (string)$ogrn;
        $this->name = (string)$name;
        $this->address = (string)$address;
        $this->comment = (string)$comment;
        $this->sum = (string)$sum;
        $this->date = (string)$date;
        $this->instance = (string)$instance;
        $this->number = (string)$number;
    }


}