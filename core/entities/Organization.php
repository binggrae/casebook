<?php

namespace core\entities;

use core\helpers\OrgHelper;

/**
 * This is the model class for table "organization".
 *
 * @property int $id
 * @property string $type
 * @property string $ogrn
 * @property string $name
 * @property string $address
 * @property string $email
 * @property string $phone
 * @property string $chief
 * @property integer $status
 *
 * @property OrgRequest[] $orgRequests
 */
class Organization extends \yii\db\ActiveRecord
{

    public static function create(\core\parser\elements\Organization $org)
    {
        $model = new self([
            'type' => 'org',
            'ogrn' => $org->ogrn,
            'name' => $org->name,
            'address' => $org->address,
            'email' => $org->email,
            'phone' => $org->phone,
            'chief' => $org->chief,
            'status' => OrgHelper::STATUS_WAIT
        ]);
        return $model;
    }


    public static function tableName()
    {
        return 'organization';
    }


    public function rules()
    {
        return [
            [['status'], 'integer'],
            [['type', 'ogrn', 'name', 'address', 'email', 'phone', 'chief'], 'string', 'max' => 255],
        ];
    }


    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'type' => 'Type',
            'ogrn' => 'ОГРН',
            'name' => 'Название',
            'address' => 'Адрес',
            'email' => 'Email',
            'phone' => 'Телефон',
            'chief' => 'Директор',
            'status' => 'Статус',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getOrgRequests()
    {
        return $this->hasMany(OrgRequest::className(), ['org_id' => 'id']);
    }

    public function getOrgRequest()
    {
        return $this->hasOne(OrgRequest::className(), ['org_id' => 'id']);
    }
}
