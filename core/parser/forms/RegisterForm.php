<?php


namespace core\parser\forms;


use yii\base\Model;

class RegisterForm extends Model
{

    public $email;
    public $name = 'Иванов Дмитрий';
    public $phone;
    public $code;

    public function getPostData()
    {
        return [
            'Email' => $this->email,
            'FullName' => $this->name,
            'Password' => '',
            'Pin' => $this->code,
            'Phone' => $this->phone,
            'SystemName' => 'Sps',
        ];
    }

//Email
//:
//"casebook_5a7d776b28e65@p33.org"
//FullName
//:
//"test test"
//Password
//:
//""
//Phone
//:
//"+79298019134"
//Pin
//:
//"6038"
//SystemName
//:
//"Sps"

}