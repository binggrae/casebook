<?php

namespace core\entities;

use core\forms\AccountForm;

/**
 * This is the model class for table "account".
 *
 * @property int $id
 * @property string $login
 * @property string $password
 * @property int $created_at
 * @property int $is_main
 */
class Account extends \yii\db\ActiveRecord
{

    public static function create(AccountForm $form)
    {
        $account = new self();
        $account->login = $form->login;
        $account->password = $form->password;
        $account->is_main = $form->is_main;
        $account->created_at = time();

        return $account;
    }

    public function edit(AccountForm $form)
    {
        $this->login = $form->login;
        $this->password = $form->password;
        $this->is_main = $form->is_main;
        $this->created_at = time();

        return $this;
    }


    public static function setMain($id, $value)
    {
        if($value) {
            Account::updateAll(['is_main' => 0], ['!=', 'id', $id]);
            Account::updateAll(['is_main' => 1], ['id' => $id]);
        } else {
            Account::updateAll(['is_main' => 0], ['id' => $id]);
            if(!Account::find()->where(['is_main' => 1])->count()) {
                $model = Account::find()->orderBy(['id' => SORT_DESC])->one();
                $model->is_main = 1;
                $model->save();
            }
        }
    }

    public static function tableName()
    {
        return 'account';
    }


    public function rules()
    {
        return [
            [['created_at', 'is_main'], 'integer'],
            [['login', 'password'], 'string', 'max' => 255],
        ];
    }


    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'login' => 'Логин',
            'password' => 'Пароль',
            'created_at' => 'Дата создания',
            'is_main' => 'По умолчанию',
        ];
    }
}
