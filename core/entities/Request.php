<?php

namespace core\entities;

use core\forms\RequestForm;
use core\helpers\OrgHelper;
use core\helpers\RequestHelper;

/**
 * This is the model class for table "request".
 *
 * @property int $id
 * @property int $status
 * @property int $type
 * @property int $sum
 * @property int $created_at
 * @property int $updated_at
 * @property string $date_from
 * @property string $date_to
 *
 * @property OrgRequest[] $orgRequests
 * @property Request[] $requests
 * @property Request[] $completeRequests
 */
class Request extends \yii\db\ActiveRecord
{

    public static function create(RequestForm $form)
    {
        $request = new self();
        $request->status = RequestHelper::STATUS_WAIT;
        $request->created_at = time();
        $request->updated_at = time();
        $request->date_from = $form->date_from;
        $request->date_to = $form->date_to;
        $request->type = $form->type;
        $request->sum = $form->sum;

        return $request;
    }

    public static function tableName()
    {
        return 'request';
    }


    public function rules()
    {
        return [
            [['status', 'created_at', 'updated_at', 'type', 'sum'], 'integer'],
            [['date_from', 'date_to'], 'required'],
            [['date_from', 'date_to'], 'string', 'max' => 255],
        ];
    }


    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'status' => 'Статус',
            'type' => 'Результат дела',
            'sum' => 'Сумма иска',
            'created_at' => 'Дата выборки',
            'updated_at' => 'Дата Изменения',
            'date_from' => 'Date From',
            'date_to' => 'Date To',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getOrgRequests()
    {
        return $this->hasMany(OrgRequest::className(), ['request_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getRequests()
    {
        return $this->hasMany(Organization::className(), ['id' => 'org_id'])
            ->viaTable(OrgRequest::tableName(), ['request_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCompleteRequests()
    {
        return $this->hasMany(Organization::className(), ['id' => 'org_id'])
            ->viaTable(OrgRequest::tableName(), ['request_id' => 'id'])
            ->andWhere([Organization::tableName() . '.status' => OrgHelper::STATUS_COMPLETE]);
    }


}
