<?php

use yii\db\Migration;

/**
 * Class m180201_122746_request
 */
class m180201_122746_request extends Migration
{
    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            // http://stackoverflow.com/questions/766809/whats-the-difference-between-utf8-general-ci-and-utf8-unicode-ci
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }

        $this->createTable('request', [
            'id' => $this->primaryKey(),
            'status' => $this->integer()->notNull()->defaultValue(0),
            'created_at' => $this->integer()->notNull()->defaultValue(0),
            'updated_at' => $this->integer()->notNull()->defaultValue(0),
            'date_from' => $this->string()->notNull(),
            'date_to' => $this->string()->notNull(),
        ], $tableOptions);


        $this->createTable('organization', [
            'id' =>$this->primaryKey(),
            'type' => $this->string(),
            'orgn' => $this->string(),
            'name' => $this->string(),
            'address' => $this->string(),
            'email' => $this->string(),
            'phone' => $this->string(),
            'chief' => $this->string(),
        ], $tableOptions);

        $this->createTable('org_request', [
            'request_id' => $this->integer()->notNull(),
            'org_id' => $this->integer()->notNull(),
        ]);

        $this->addForeignKey('fk_orgRequest_request',
            'org_request', 'request_id',
        'request', 'id',
        'CASCADE', 'RESTRICT'
        );

        $this->addForeignKey('fk_orgRequest_organization',
            'org_request', 'org_id',
            'organization', 'id',
            'CASCADE', 'RESTRICT'
        );

    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        echo "m180201_122746_request cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m180201_122746_request cannot be reverted.\n";

        return false;
    }
    */
}
